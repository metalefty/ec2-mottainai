ec2-mottainai
=============

Amazon EC2 の自動起動、自動停止を支援するツールです。
開発サーバなどを就業時間中だけ動作させるために使います。

使い方
------
このリポジトリを clone して `bundle install` します。

AWS のアクセスキーは環境変数から与えます。
`AWS_ACCESS_KEY_ID` と `AWS_SECRET_ACCESS_KEY` を予めセットしてください。

```
Usage: mottainai [options]
    -i, --instance-id INSTANCEID
    -a, --action ACTION
    -r, --region [REGION]

コマンドラインオプション:
 -i, --instnace-id インスタンスIDを指定(複数可)
 -a, --action アクションを指定 (stop, start, terminate)
 -r, --region リージョンを指定 (指定がない場合は ap-northeast-1)
```

実際の使用例
------------

`ap-northeast-1` のインスタンス `i-61670242` `i-f358a01a` `i-126b98c6e` をストップします。
```
$ ./mottainai -a stop -i i-61670242 -i i-f358a01a -i i-26b98c6e
```

同じく3つのインスタンスをスタートします。
```
$ ./mottainai -a start -i i-61670242 -i i-f358a01a -i i-26b98c6e
```


同じく3つのインスタンスをターミネートします。
```
$ ./mottainai -a terminate -i i-61670242 -i i-f358a01a -i i-26b98c6e
```

指定した時刻に実行する機能は持っていないため、 `cron` を使って実現します。

``` shell
AWS_ACCESS_KEY_ID=foo
AWS_SECRET_ACCESS_KEY=bar
PATH=~ec2-user/.rbenv/shims:/sbin:/bin:/usr/sbin:/usr/bin
# 毎週月〜金曜日の午前9時に i-61670242 をスタートする。
0 9 * * * 1,2,3,4,5 ~ec2-user/ec2-mottainai/mottainai -a start -i i-61670242
# 毎週月〜金曜日の午後6時に i-61670242 をストップする。
0 18 * * * 1,2,3,4,5 ~ec2-user/ec2-mottainai/mottainai -a start -i i-61670242
```

複数の AWS アクセスキーを使い分けたい場合は、`env` コマンドを使用して環境変数をセットします。
``` shell
PATH=~ec2-user/.rbenv/shims:/sbin:/bin:/usr/sbin:/usr/bin
# 毎週月〜金曜日の午前9時に i-61670242 をスタートする。
0 9 * * * 1,2,3,4,5 env AWS_ACCESS_KEY_ID=foo1 AWS_SECRET_ACCESS_KEY=bar1 ~ec2-user/ec2-mottainai/mottainai -a start -i i-61670242
# 毎週月〜金曜日の午後6時に i-61670242 をストップする。
0 18 * * * 1,2,3,4,5 env AWS_ACCESS_KEY_ID=foo2 AWS_SECRET_ACCESS_KEY=bar2 ~ec2-user/ec2-mottainai/mottainai -a start -i i-61670242
```
